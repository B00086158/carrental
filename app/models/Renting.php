<?php

class Renting extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="collectdate", type="string", nullable=true)
     */
    protected $collectdate;

    /**
     *
     * @var string
     * @Column(column="returndate", type="string", nullable=true)
     */
    protected $returndate;

    /**
     *
     * @var integer
     * @Column(column="customerid", type="integer", length=11, nullable=true)
     */
    protected $customerid;

    /**
     *
     * @var integer
     * @Column(column="vehicleid", type="integer", length=11, nullable=true)
     */
    protected $vehicleid;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field collectdate
     *
     * @param string $collectdate
     * @return $this
     */
    public function setCollectdate($collectdate)
    {
        $this->collectdate = $collectdate;

        return $this;
    }

    /**
     * Method to set the value of field returndate
     *
     * @param string $returndate
     * @return $this
     */
    public function setReturndate($returndate)
    {
        $this->returndate = $returndate;

        return $this;
    }

    /**
     * Method to set the value of field customerid
     *
     * @param integer $customerid
     * @return $this
     */
    public function setCustomerid($customerid)
    {
        $this->customerid = $customerid;

        return $this;
    }

    /**
     * Method to set the value of field vehicleid
     *
     * @param integer $vehicleid
     * @return $this
     */
    public function setVehicleid($vehicleid)
    {
        $this->vehicleid = $vehicleid;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field collectdate
     *
     * @return string
     */
    public function getCollectdate()
    {
        return $this->collectdate;
    }

    /**
     * Returns the value of field returndate
     *
     * @return string
     */
    public function getReturndate()
    {
        return $this->returndate;
    }

    /**
     * Returns the value of field customerid
     *
     * @return integer
     */
    public function getCustomerid()
    {
        return $this->customerid;
    }

    /**
     * Returns the value of field vehicleid
     *
     * @return integer
     */
    public function getVehicleid()
    {
        return $this->vehicleid;
    }
    public function getDuration()
    {
    $duration = new DateTime($this->Collectdate);
    $today = new DateTime($this->Returndate);
    $interval = $today->diff($duration);
    return $interval->format("%d");
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("B00086158");
        $this->setSource("Renting");
        $this->belongsTo('customerid', '\Customer', 'id', ['alias' => 'Customer']);
        $this->belongsTo('vehicleid', '\Vehicle', 'id', ['alias' => 'Vehicle']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Renting';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Renting[]|Renting|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Renting|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
