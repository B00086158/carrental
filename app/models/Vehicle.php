<?php

class Vehicle extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="make", type="string", length=30, nullable=true)
     */
    protected $make;

    /**
     *
     * @var string
     * @Column(column="model", type="string", length=30, nullable=true)
     */
    protected $model;

    /**
     *
     * @var string
     * @Column(column="regNumber", type="string", length=30, nullable=true)
     */
    protected $regNumber;

    /**
     *
     * @var string
     * @Column(column="colour", type="string", length=30, nullable=true)
     */
    protected $colour;

    /**
     *
     * @var string
     * @Column(column="rentPrice", type="string", length=30, nullable=true)
     */
    protected $rentPrice;

    /**
     *
     * @var string
     * @Column(column="available", type="string", length=30, nullable=true)
     */
    protected $available;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field make
     *
     * @param string $make
     * @return $this
     */
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Method to set the value of field model
     *
     * @param string $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Method to set the value of field regNumber
     *
     * @param string $regNumber
     * @return $this
     */
    public function setRegNumber($regNumber)
    {
        $this->regNumber = $regNumber;

        return $this;
    }

    /**
     * Method to set the value of field colour
     *
     * @param string $colour
     * @return $this
     */
    public function setColour($colour)
    {
        $this->colour = $colour;

        return $this;
    }

    /**
     * Method to set the value of field rentPrice
     *
     * @param string $rentPrice
     * @return $this
     */
    public function setRentPrice($rentPrice)
    {
        $this->rentPrice = $rentPrice;

        return $this;
    }

    /**
     * Method to set the value of field available
     *
     * @param string $available
     * @return $this
     */
    public function setAvailable($available)
    {
        $this->available = $available;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field make
     *
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Returns the value of field model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Returns the value of field regNumber
     *
     * @return string
     */
    public function getRegNumber()
    {
        return $this->regNumber;
    }

    /**
     * Returns the value of field colour
     *
     * @return string
     */
    public function getColour()
    {
        return $this->colour;
    }

    /**
     * Returns the value of field rentPrice
     *
     * @return string
     */
    public function getRentPrice()
    {
        return $this->rentPrice;
    }

    /**
     * Returns the value of field available
     *
     * @return string
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("B00086158");
        $this->setSource("Vehicle");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Vehicle';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Vehicle[]|Vehicle|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Vehicle|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
