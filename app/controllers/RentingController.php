<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class RentingController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for Renting
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Renting', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $renting = Renting::find($parameters);
        if (count($renting) == 0) {
            $this->flash->notice("The search did not find any Renting");

            $this->dispatcher->forward([
                "controller" => "Renting",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $renting,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a Renting
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $renting = Renting::findFirstByid($id);
            if (!$renting) {
                $this->flash->error("Renting was not found");

                $this->dispatcher->forward([
                    'controller' => "Renting",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $renting->getId();

            $this->tag->setDefault("id", $renting->getId());
            $this->tag->setDefault("collectdate", $renting->getCollectdate());
            $this->tag->setDefault("returndate", $renting->getReturndate());
            $this->tag->setDefault("customerid", $renting->getCustomerid());
            $this->tag->setDefault("vehicleid", $renting->getVehicleid());
            
        }
    }

    /**
     * Creates a new Renting
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "Renting",
                'action' => 'index'
            ]);

            return;
        }

        $renting = new Renting();
        $renting->setcollectdate($this->request->getPost("collectdate"));
        $renting->setreturndate($this->request->getPost("returndate"));
        $renting->setcustomerid($this->request->getPost("customerid"));
        $renting->setvehicleid($this->request->getPost("vehicleid"));
        

        if (!$renting->save()) {
            foreach ($renting->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "Renting",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Renting was created successfully");

        $this->dispatcher->forward([
            'controller' => "Renting",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a Renting edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "Renting",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $renting = Renting::findFirstByid($id);

        if (!$renting) {
            $this->flash->error("Renting does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "Renting",
                'action' => 'index'
            ]);

            return;
        }

        $renting->setcollectdate($this->request->getPost("collectdate"));
        $renting->setreturndate($this->request->getPost("returndate"));
        $renting->setcustomerid($this->request->getPost("customerid"));
        $renting->setvehicleid($this->request->getPost("vehicleid"));
        

        if (!$renting->save()) {

            foreach ($renting->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "Renting",
                'action' => 'edit',
                'params' => [$renting->getId()]
            ]);

            return;
        }

        $this->flash->success("Renting was updated successfully");

        $this->dispatcher->forward([
            'controller' => "Renting",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a Renting
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $renting = Renting::findFirstByid($id);
        if (!$renting) {
            $this->flash->error("Renting was not found");

            $this->dispatcher->forward([
                'controller' => "Renting",
                'action' => 'index'
            ]);

            return;
        }

        if (!$renting->delete()) {

            foreach ($renting->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "Renting",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("Renting was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "Renting",
            'action' => "index"
        ]);
    }

}
