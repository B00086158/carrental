<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class VehicleController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for Vehicle
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Vehicle', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $vehicle = Vehicle::find($parameters);
        if (count($vehicle) == 0) {
            $this->flash->notice("The search did not find any Vehicle");

            $this->dispatcher->forward([
                "controller" => "Vehicle",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $vehicle,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a Vehicle
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $vehicle = Vehicle::findFirstByid($id);
            if (!$vehicle) {
                $this->flash->error("Vehicle was not found");

                $this->dispatcher->forward([
                    'controller' => "Vehicle",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $vehicle->getId();

            $this->tag->setDefault("id", $vehicle->getId());
            $this->tag->setDefault("make", $vehicle->getMake());
            $this->tag->setDefault("model", $vehicle->getModel());
            $this->tag->setDefault("regNumber", $vehicle->getRegnumber());
            $this->tag->setDefault("colour", $vehicle->getColour());
            $this->tag->setDefault("rentPrice", $vehicle->getRentprice());
            $this->tag->setDefault("available", $vehicle->getAvailable());
            
        }
    }

    /**
     * Creates a new Vehicle
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "Vehicle",
                'action' => 'index'
            ]);

            return;
        }

        $vehicle = new Vehicle();
        $vehicle->setmake($this->request->getPost("make"));
        $vehicle->setmodel($this->request->getPost("model"));
        $vehicle->setregNumber($this->request->getPost("regNumber"));
        $vehicle->setcolour($this->request->getPost("colour"));
        $vehicle->setrentPrice($this->request->getPost("rentPrice"));
        $vehicle->setavailable($this->request->getPost("available"));
        

        if (!$vehicle->save()) {
            foreach ($vehicle->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "Vehicle",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Vehicle was created successfully");

        $this->dispatcher->forward([
            'controller' => "Vehicle",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a Vehicle edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "Vehicle",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $vehicle = Vehicle::findFirstByid($id);

        if (!$vehicle) {
            $this->flash->error("Vehicle does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "Vehicle",
                'action' => 'index'
            ]);

            return;
        }

        $vehicle->setmake($this->request->getPost("make"));
        $vehicle->setmodel($this->request->getPost("model"));
        $vehicle->setregNumber($this->request->getPost("regNumber"));
        $vehicle->setcolour($this->request->getPost("colour"));
        $vehicle->setrentPrice($this->request->getPost("rentPrice"));
        $vehicle->setavailable($this->request->getPost("available"));
        

        if (!$vehicle->save()) {

            foreach ($vehicle->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "Vehicle",
                'action' => 'edit',
                'params' => [$vehicle->getId()]
            ]);

            return;
        }

        $this->flash->success("Vehicle was updated successfully");

        $this->dispatcher->forward([
            'controller' => "Vehicle",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a Vehicle
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $vehicle = Vehicle::findFirstByid($id);
        if (!$vehicle) {
            $this->flash->error("Vehicle was not found");

            $this->dispatcher->forward([
                'controller' => "Vehicle",
                'action' => 'index'
            ]);

            return;
        }

        if (!$vehicle->delete()) {

            foreach ($vehicle->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "Vehicle",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("Vehicle was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "Vehicle",
            'action' => "index"
        ]);
    }

}
